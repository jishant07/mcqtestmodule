<!DOCTYPE html>
<html>
<head>
	<title>THE TEST PAGE!!</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		*
		{
			font-family: Roboto;
		}
		.option
		{
			border:2px solid darkblue;
			border-radius:10px;
			font-size:20px;
			padding: 5px; 
			margin: 10px auto;
			text-align:left;
			width:50%;
		}
		.jumbotron
		{
			padding:2em !important;
			text-align:center;
			width:80%;
			margin:auto;
		}
		.option:hover
		{
			background:lightgreen;
			transition:0.3s;
			-webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
			-moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
			box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
		}	
		#question
		{
			border-radius:10px;
			padding: 5px;
			margin: 5px auto;
			text-align:left !important;
			width:50%;	
			font-size:24px; 
		}
		.questionback
		{
			background:white;
			width: 80%;
			margin: auto;
			text-align:center;
			border-radius:10px;
		}
		body
		{
			background:url('./assets/images/1.jpg');
			background-size:cover;
			background-repeat:no-repeat;
			background-position:center;
			background-attachment:fixed;
		}
		.btn
		{
			width:80% !important;
			margin:auto;
			text-align: center;	
		}
	</style>
</head>
<body>
	<div class="container">
		<br>
		<br>
		<div class="jumbotron">
			<h1>Database quiz</h1>
		</div>
		<br>
		<br>
	</div>
	<?php
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "node_dataset";
	$con = new mysqli($servername,$username,$password,$dbname);
	if($con->connect_error)
		{
			echo "$con->connect_error";
		}
		echo "<div class='container'>
						<form action='result.php' method='post'><div class='questionback'>";
		for ($srno=1; $srno <=10 ; $srno++)
		{ 
			$sql = "select * from table_2 where srno='$srno'";
			$result = $con->query($sql);
			while($row = $result->fetch_assoc())
			{
				$name = "answer".$srno;
				$question = $row["question"];
				$opt1 = $row["opt1"];
				$opt2 = $row["opt2"];
				$opt3 = $row["opt3"];
				$opt4 = $row["opt4"];
					echo "
						<br>	
						<h3 id='question'>$question</h3>
						<div class='option'>
						<input type='radio' name='$name' value='$opt1' required>   $opt1
						</div>
						<div class='option'>
						<input type='radio' name='$name' value='$opt2' required>   $opt2
						</div>
						<div class='option'>
						<input type='radio' name='$name' value='$opt3' required>   $opt3
						</div>
						<div class='option'>
						<input type='radio' name='$name' value='$opt4' required>   $opt4
						</div>
						<br>";
			}
		}
		echo "<br>
			  <br>
				<button class='btn btn-primary btn-lg'>Submit</button>
				<br>
				<br>
			</form></div></div><br>";
	?>
</body>
</html>
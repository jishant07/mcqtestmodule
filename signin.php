<!DOCTYPE html>
<html>
<head>
	<?php include './partials/header.php' ?>
	<style type="text/css">
		body
		{
			background:url('https://images.unsplash.com/photo-1551313323-f7772874ab67?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1392&q=80');
			background-size:cover;
			background-repeat:no-repeat;
			background-position:center;
			background-attachment:fixed;
		}
		form
		{
			-webkit-box-shadow: 0px 0px 20px 5px rgba(0,0,0,0.75);
			-moz-box-shadow: 0px 0px 20px 5px rgba(0,0,0,0.75);
			box-shadow: 0px 0px 20px 5px rgba(0,0,0,0.75);
			border-radius:10px;
		}
	</style>
</head>
<body>
	<div class="container">
		<form class="grid-flex" action="usercheck.php" method="post" style="width:50%;text-align:center;margin:auto;margin-top:200px;background:white;"> 
			<br>
			<br>
			<div class="column w-100">
				<div class="input-animation">
					<label for="email">Email</label>
					<input type="email" id="email" name="email" required autofocus/>
				</div>
			</div>
			<br>
			<br>
			<div class="column w-100">
				<div class="input-animation">
					<label for="password">Password</label>
					<input type="password" id="pwd" name="pwd" required />
				</div>
			</div>
			<br>
			<br>
			<div class="column w-100">
				<button type="submit" class="btn bg-main-color icon-send">Check</button>
			</div>
		</form>
	</div>
</body>
<?php include './partials/footer.php' ?>
</html>
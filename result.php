<!DOCTYPE html>
<html>
<head>
	<?php include './partials/header.php' ?>
	<style type="text/css">
		.result_format
		{
			width:70%;
			margin:auto;
			background: white;
			border-radius:10px;
			-webkit-box-shadow: 0px 0px 30px 10px rgba(0,0,0,0.75);
			-moz-box-shadow: 0px 0px 30px 10px rgba(0,0,0,0.75);
			box-shadow: 0px 0px 30px 10px rgba(0,0,0,0.75);
		}
		.jumbotron
		{
			text-align: center;
			width: 70%;
			margin:auto;
		}
		.correct
		{
			text-align:center;
			background:lightgreen;
			margin:auto;
			width:50%;
			padding:30px;
			border-radius:10px;
			color:white;
		}
		.wrong
		{
			text-align:center;
			background:rgb(150,0,0);
			margin:auto;
			width:50%;
			padding:30px;
			border-radius:10px;
			color:white;
		}
		.question
		{
			width:70%;
			margin:auto;
		}
		.score
		{
			text-align:center;
			margin:auto;
			width:30%;
		}
		.pass
		{
			text-align:center;
			margin:auto;
			width:30%;
			background:lightgreen;
			color:white;
		}
		.fail
		{
			text-align:center;
			margin:auto;
			width:30%;
			background:red;
			color:white;
		}
		.logout
		{
			text-align:center;
			margin:auto;
		}
		body
		{
			background:url('https://images.unsplash.com/photo-1518133835878-5a93cc3f89e5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1575&q=80');
			background-size:cover;
			background-repeat:no-repeat;
			background-position:center;
			background-attachment:fixed;
		}
	</style>
</head>
<body>
	<div class="container">
		<br>
		<br>
		<div class="jumbotron">
			<h2>Check how you did</h3>
		</div>
		<br>
		<br>
	<div class="result_format">
		<br>
	<?php 
	session_start();
		$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "node_dataset";
	$score = 0;	
	$con = new mysqli($servername,$username,$password,$dbname);
		for($srno=1;$srno<=10;$srno++)
		{
			$selected_option = $_POST["answer".$srno];
			$sql = "select * from table_2 where srno=$srno";
			$result = $con->query($sql);
			$row = $result->fetch_assoc();
			if($row['answer'] == $selected_option)
			{
				echo "<br><h3 class='question'>".$srno."  ".$row['question']."</h3>";
				echo "<br><p class='correct'>".$selected_option." is correct</p>";
				$score = $score + 1;
			}
			else
			{
				echo "<br><h3 class='question'>".$srno."  ".$row['question']."</h3>";
				echo "<br><p class='wrong'>".$selected_option." is incorrect</p>";	
				echo "<br><p class='correct'>".$row['answer']." is the answer</p>";
			}
		}
		echo "<br><br>";
		if($score<5)
		{
			echo "<h2 class='score'>Your score is : ".$score."</h2><br>";
			echo "<h3 class='fail'>You have failed your exam</h3>";
		}
		else
		{
			echo "<h2 class='score'>Your score is : ".$score."</h2><br>";
			echo "<h3 class='pass'>You passed the exam</h3>";
		}
		$email = $_SESSION['email'];
		$insert_sql = "INSERT INTO `results`(`user`, `score`) VALUES ('$email',$score)";
		$con->query($insert_sql);
	?>
	<br>
	<br>
	<div class="logout">
		<a href="logout.php"><button class="btn btn-danger btn-md">LogOut</button></a>
	</div>
	<br>
	<br>
</div>
<br>
<br>
</div>
</body>
</html>
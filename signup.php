<!DOCTYPE html>
<html>
<head>
	<?php include './partials/header.php' ?>
	<style type="text/css">
		*
		{
			font-size:20px;
			font-family: Roboto;
		}
		.container
		{
			width: 50%;
			margin:auto;
			text-align:center;
			-webkit-box-shadow: 0px 0px 30px 10px rgba(0,0,0,0.75);
			-moz-box-shadow: 0px 0px 30px 10px rgba(0,0,0,0.75);
			box-shadow: 0px 0px 30px 10px rgba(0,0,0,0.75);
			border-radius: 10px;
		}
		.jumbotron
		{
			width:80%;
			text-align: center;
			margin: auto;
			border-radius:10px;
		}
	</style>
<body>
	<br>
	<br>
	<div class="jumbotron">
		<h1>Sign Up to take the test</h1>
	</div>
	<br>
	<br>
	<div class="container">
		<br>
		<br>
		<div>
			<form class="grid-flex" method="post" action="adduser.php">
	<div class="column w-50">
		<div class="input-animation">
			<label for="first-name">First name</label>
			<input type="text" id="first-name" name="fname" required />
		</div>
	</div>
	<div class="column w-50">
		<div class="input-animation">
			<label for="last-name">Last name</label>
			<input type="text" id="last-name" name="lname" required />
		</div>
	</div>
	<div class="column w-100">
		<div class="input-animation">
			<label for="email">Email</label>
			<input type="email" id="email" name="email" required />
		</div>
	</div>
	<div class="column w-50">
		<div class="input-animation">
			<label for="Age">Age</label>
			<input type="number" id="age" name="age" required />
		</div>
	</div>
	<div id="agemessage"></div>
	<div class="column w-50">
		<div class="input-animation">
			<label for="mobile-number">Mobile Number</label>
			<input type="number" id="mobnum" name="mobnum" required />
		</div>
	</div>
	<div class="column w-100">
		<div class="input-animation">
			<label for="Password">Password</label>
			<input type="password" id="pwd" name="pwd" required />
		</div>
	</div>
	<div class="column w-100">
		<div class="input-animation">
			<label for="Re-enter Password">Re-enter Password</label>
			<input type="password" id="repwd" name="repwd" required />
		</div>
	</div>
	<div id="pwdmessage"></div>
	<div class="column w-100">
		<button type="submit" class="btn bg-main-color icon-send" id="submit">Send</button>
	</div>
</form>
		</div>
	</div>
	<br><br>
</body>
<?php include './partials/footer.php' ?>
<script type="text/javascript">
	$("#repwd").on("focusout",function()
	{
		var pwd = $("#pwd").val();
		var repwd = $("#repwd").val();
		if(pwd == "" || repwd == "")
		{
			$("#pwdmessage").html("<h4>Passwords Empty</h4>");	
		}
		else if (pwd === repwd)
		{
			$("#pwdmessage").html("<h4>Passwords Match</h4>");
		}
		else
		{
			$("#pwd").val("");
			$("#repwd").val("");
			$("#pwdmessage").html("<h4>Passwords do not Match</h4>");
		}
	});
	$('#age').on("focusout",function(req,res)
	{

		var age = parseInt($('#agemessage').val());
		if(age<=0)
		{
			$('#agemessage').html("<h4>Age cannot be zero or negative</h4>");
		}
	});
</script>
</html>
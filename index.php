<!DOCTYPE html>
<html>
<head>
	<?php include './partials/header.php' ?>
	<style type="text/css">
		.jumbotron
		{
			text-align: left;
			color: black;
			margin:15% auto; 
			text-align:center;
		}
		body
		{
			background : url('https://images.unsplash.com/photo-1525373698358-041e3a460346?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1400&q=80');
			background-size: cover;
			background-position: center;
		}
		hr.style-five 
		{
    		border: 0;
    		height: 0;
    		width:400px;
		}
		hr.style-five:after 
		{  
    		content: "\00a0";
		}
		html
		{
			height:100%;
		}
	</style>
</head>
<body>
<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#ABC" aria-expanded="false">
	        	<span class="sr-only">Toggle navigation</span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	      		</button>
				<a href="/" class="navbar-brand">TEST</a>
			</div>	
			 <div class="collapse navbar-collapse" id="ABC">
				<ul class="nav navbar-nav">
					<li><a href="/">Home</a></li>
				</ul>
			
				<ul class="nav navbar-nav navbar-right">
					<li><a href="signin.php">Sign-In <i class="fa fa-user-plus" aria-hidden="true"></i></a></li>
					<li><a href="signup.php">Sign-Up <i class="fa fa-user-plus" aria-hidden="true"></i></a></li>	

				</ul>
			</div>
			</div>
		</div>
	</nav>
	 <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<div class="container">
		<div class="jumbotron">
			<h1>Database Test!</h1>
		</div>
	</div>
</body>
<?php include './partials/footer.php' ?>
</html>
<!DOCTYPE html>
<html>
<head>
	<?php include './partials/header.php' ?>
	<style type="text/css">
		.jumbotron , .row , .logout
		{
			width:80%;
			margin:auto;
			text-align:center;
		}
		.row
		{
			background:white;
			border-radius: 10px;
		}
		*
		{
			font-family: Roboto;
		}
		.score_display
		{
			width:80%;
			margin:auto;
			text-align:center;
			color: white;
		}
		body
		{
			background:url('https://images.unsplash.com/photo-1488590528505-98d2b5aba04b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80');
			background-size:cover;
			background-repeat:no-repeat;
			background-position:center;
			background-attachment:fixed;
		}
	</style>
</head>
<body>
	<div class="container">
		<br>
		<br>
		<div class="jumbotron">
<?php 
	session_start();
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "node_dataset";
	$con = new mysqli($servername,$username,$password,$dbname);
	if($_SESSION['isLoggedIn'])
	{
	$email = $_SESSION['email'];
	$fetch_sql = "select * from User where email='$email';";
	$result = $con->query($fetch_sql);
	$row = $result->fetch_assoc();
	echo "	<h3 style='color:black'>".$row['first_name']."'s Details</h3>
</div><br><br>
	<div class='row'>
		<div class='col-lg-6'>
			<h3>First Name : ".$row['first_name']."</h3>
			<h3>Email : ".$row['email'] ."</h3>
		</div>
		<div class='col-lg-6'>
			<h3>Last Name : ".$row['last_name']."</h3>
			<h3>Mobile Number :".$row['mobnum']."</h3>
		</div>
		<div class='col-lg-12'>
			<h3>Age : ".$row['age']."</h3>
		</div>
		<div class='col-lg-12'>
			<a href='test.php'><button class='btn btn-lg btn-primary'>Click here to take the test</button></a>
		</div>
	</div>
	<br>
	<br>
</div>";
	$sql1 = "select * from results where user='$email';";
	$result1 = $con->query($sql1);
	echo "<div class='score_display'>";
	echo "<br><h2>Tests already given : ".$result1->num_rows." </h2><br>";
	if($result1->num_rows>0)
	{
		while($row = $result1->fetch_assoc())	
		{
			echo "<h3> Previous Score ".$row['score']." </h3>";
		}
	}
	else
	{
		echo "<h3>No tests taken yet.</h3>";
	}
	echo "</div>";

}
else
{
	header("Location:signin.php");
}
?>
<div class="logout">
	<a href="logout.php"><button class="btn btn-danger btn-md">LogOut</button></a>
</div>
</body>
</html>
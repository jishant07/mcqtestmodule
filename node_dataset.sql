-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2019 at 09:39 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `node_dataset`
--

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `user` text NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`user`, `score`) VALUES
('rahulgupta1597@gmail.com', 3),
('rahulgupta1597@gmail.com', 3);

-- --------------------------------------------------------

--
-- Table structure for table `table_2`
--

CREATE TABLE `table_2` (
  `question` text NOT NULL,
  `opt1` varchar(100) NOT NULL,
  `opt2` varchar(100) NOT NULL,
  `opt3` varchar(100) NOT NULL,
  `opt4` varchar(100) NOT NULL,
  `srno` int(10) DEFAULT NULL,
  `answer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_2`
--

INSERT INTO `table_2` (`question`, `opt1`, `opt2`, `opt3`, `opt4`, `srno`, `answer`) VALUES
('ALTER TABLE `table_2` CHANGE `question` `question` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;', 'DML(Data Manipulation Langauge)', 'DDL(Data Definition Langauge)', 'Query', 'Relational Schema', 1, 'DDL(Data Definition Langauge)'),
('Which one of the following provides the ability to query information from the database and to insert tuples into, delete tuples from, and modify tuples in the database?', 'DML(Data Manipulation Langauge', 'DDL(Data Definition Langauge)', 'Query', 'Relational Schema', 2, 'DML(Data Manipulation Langauge'),
('CREATE TABLE employee (name VARCHAR, id INTEGER)\r\nWhat type of statement is this?', 'DML', 'DDL', 'View', 'Integrity constraint', 3, 'DDL'),
('CREATE TABLE employee (name VARCHAR, id INTEGER)\r\nWhat type of statement is this?', 'DML', 'DDL', 'View', 'Integrity constraint', 4, 'DDL'),
('SELECT * FROM employee\r\nWhat type of statement is this?', ' DML', 'DDL', 'View', 'Integrity constraint', 5, ' DML'),
('The basic data type char(n) is a _____ length character string and varchar(n) is _____ length character.', 'Fixed, equal', 'Equal, variable', 'Fixed, variable', 'Variable, equal', 6, 'Fixed, variable'),
('An attribute A of datatype varchar(20) has the value “Avi”. The attribute B of datatype char(20) has value ”Reed”. Here attribute A has ____ spaces and attribute B has ____ spaces.', '3, 20', '20, 4', '20, 20', '3, 4', 7, '3, 20'),
('To remove a relation from an SQL database, we use the ______ command.', 'Delete', 'Purge', 'Remove', 'Drop table', 8, 'Drop table'),
('DELETE FROM r;   //r - relation\r\nThis command performs which of the following action?', 'Remove relation', 'Clear relation entries', 'Delete fields', 'Delete rows', 9, 'Clear relation entries'),
('INSERT INTO instructor VALUES (10211, ’Smith’, ’Biology’, 66000);\r\nWhat type of statement is this?', 'Query', 'DML', 'Relational', 'DDL', 10, 'DML');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `email` text NOT NULL,
  `age` int(10) NOT NULL,
  `mobnum` int(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`first_name`, `last_name`, `email`, `age`, `mobnum`, `password`) VALUES
('Prashant', 'Acharya', 'rahulgupta1597@gmail.com', 20, 2147483647, 'abcd');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
